
/** 
 * Attitude Indicator
 * Graphics2D
 * Chris Underwood 2018
 * New Zealand
 * undeze@gmail.com
 * 
 * **/

import java.awt.Canvas;

import java.awt.Dimension;

import java.awt.Graphics;
import java.awt.Graphics2D;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.*;

import java.awt.image.BufferStrategy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Date;

import java.text.DecimalFormat;

import java.awt.Color;

// $ cd workspace/AttitudeIndicator/src
// $ git add .
// $ git commit -m 'message here'
// $ git push origin master


public class AttitudeIndicator extends Canvas implements KeyListener, MouseListener, Runnable {

	private static final long serialVersionUID = 1L;
	private Graphics bufferGraphics = null; // Back buffer graphics
	private BufferStrategy bufferStrategy = null;

	private Thread thread;
	private boolean running;

	private double phi = 0.0; // roll angle
	private double theta = 0.0; // pitch attitude
	private double heading = 0.0;

	// Body rates
	private double dPitch = 0.0;
	private double dRoll = 0.0;
	private double dYaw = 0.0;

	private DecimalFormat df1 = new DecimalFormat("#.#");

	private int northY, southY;

	private int radius = 200;

	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS");

	private ArrayList<Integer> keysDown;

	private Area ndR, ndC, sky;
	private Rectangle2D ndRect;
	private Arc2D skyArc, skyTop, groundArc;

	private Area background;

	// Button positions
	private int buttX = 70, buttY = 20;
	private int p270X = 520, p270Y = 50;
	private boolean p270B = false;
	private int instrumentX = 520, instrumentY = 90;
	private boolean instrumentB = true;
	private int meridianX = 520, meridianY = 130;
	private boolean meridianB = true;
	private int parallelX = 520, parallelY = 170;
	private boolean parallelB = true;
	private int numberX = 520, numberY = 210;
	private boolean numberB = true;

	// Orientation matrix to derive aircraft attitude
	private double a11, a12, a13, a21, a22, a23, a31, a32, a33;
	// rotation matrix
	private double r11, r12, r13, r21, r22, r23, r31, r32, r33;
	// temporary matrix
	private double b11, b12, b13, b21, b22, b23, b31, b32, b33;
	// unit vectors
	private double ux, uy, uz;

	private static long time, loopComplete, deltaT;
	private static int loopCount;
	
	// Constructor
	public AttitudeIndicator(Dimension size) {

		Ellipse2D ndCircle;
		Ellipse2D groundSky;

		// Entries in the attitude matrix. Orientate wings level, heading north.
		a11 = 1;
		a12 = 0;
		a13 = 0;
		a21 = 0;
		a22 = 1;
		a23 = 0;
		a31 = 0;
		a32 = 0;
		a33 = 1;
		
		loopComplete = 0;

		ndRect = new Rectangle2D.Double(-200, -200, 401, 401);

		ndR = new Area(ndRect);

		ndCircle = new Ellipse2D.Double(-150, -150, 300, 300);
		groundSky = new Ellipse2D.Double(-200, -200, 400, 400);

		background = new Area(groundSky);

		ndC = new Area(ndCircle);

		ndR.subtract(ndC);

		keysDown = new ArrayList<Integer>();

		this.setPreferredSize(size);
		this.addKeyListener(this);
		this.addMouseListener(this);

		this.thread = new Thread(this);
		running = true;

	}

	public void paint(Graphics g) {
		// Draw on to the screen
		if (bufferStrategy == null) {
			this.createBufferStrategy(2);
			bufferStrategy = this.getBufferStrategy();
			bufferGraphics = bufferStrategy.getDrawGraphics();
			this.thread.start(); // starts thread, initiates run method
		}
	}

	public void run() {
		// This is what runs when the level editor is running
		this.requestFocus();

		while (running) {
			// Program's logic: math and calculations

			
			time = System.currentTimeMillis();
			loopCount = 0;
			while (time < loopComplete + 1){
				time = System.currentTimeMillis();
				loopCount++;
			}
			
			deltaT = time - loopComplete;
			
			// Action the body rates
			pitch();
			roll();
			yaw();

			// retrieve body angles from the orientation matrix
			heading = Math.atan2(a21, a11); // yaw
			theta = Math.atan2(a31, (Math.sqrt((a32 * a32) + (a33 * a33)))); // pitch
			phi = Math.atan2(a32, a33); // roll

			Draw();
			DrawBackbufferToScreen();

			loopComplete = System.currentTimeMillis();
			
			Thread.currentThread();
			try {
				// Thread.sleep(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void Draw() {

		bufferGraphics = bufferStrategy.getDrawGraphics();
		try {
			bufferGraphics.clearRect(0, 0, this.getSize().width, this.getSize().height);
			// Draw to back buffer
			Graphics2D globe = (Graphics2D) bufferGraphics;

			drawData(globe);

			drawButtons(globe);

			globe.translate(300, 220);

			AffineTransform old = globe.getTransform();

			globe.rotate(phi); // roll attitude angle

			globe.setColor(Color.gray);
			globe.fill(background);

			drawParallels(globe);

			draw270Pointer(globe);

			if (meridianB)
				drawMeridians(globe);

			if (numberB)
				drawNumbers(globe);

			drawPoles(globe);

			// remove rotation
			globe.setTransform(old);

			globe.setColor(Color.black);
			globe.drawOval(-200, -200, 400, 400); // boundary circle

			drawAircraft(globe);

			if (instrumentB)
				drawInstrument(globe);

		} catch (

		Exception e) {
			e.printStackTrace();
		} finally

		{
			bufferGraphics.dispose();
		}
	}

	/* Draws an attitude indicator instrument case on top of the globe */
	public void drawInstrument(Graphics2D globe) {

		globe.setColor(Color.black);
		globe.fill(ndR);

		globe.setColor(Color.darkGray);
		globe.drawOval(-radius + 30, -radius + 30, 2 * radius - 60, 2 * radius - 60);
		globe.setColor(Color.white);

		globe.drawArc(-radius + 30, -radius + 30, 2 * radius - 60, 2 * radius - 60, 60, 60);

		// Angle of bank pointer
		globe.setColor(Color.cyan);
		int AoBx01 = (int) (150 * Math.sin(phi)); // pointer
		int AoBy01 = -(int) (150 * Math.cos(phi));
		int AoBx02 = (int) (170 * Math.sin(phi));
		int AoBy02 = -(int) (170 * Math.cos(phi));
		globe.drawLine(AoBx01, AoBy01, AoBx02, AoBy02);
		AoBx01 = (int) (160 * Math.sin(phi - 0.04));
		AoBy01 = -(int) (160 * Math.cos(phi - 0.04));
		AoBx02 = (int) (170 * Math.sin(phi));
		AoBy02 = -(int) (170 * Math.cos(phi));
		globe.drawLine(AoBx01, AoBy01, AoBx02, AoBy02);
		AoBx01 = (int) (160 * Math.sin(phi + 0.04));
		AoBy01 = -(int) (160 * Math.cos(phi + 0.04));
		AoBx02 = (int) (170 * Math.sin(phi));
		AoBy02 = -(int) (170 * Math.cos(phi));
		globe.drawLine(AoBx01, AoBy01, AoBx02, AoBy02);

		// Angle of bank markers
		globe.setColor(Color.white);
		for (double i = 0; i < 2 * Math.PI; i = i + Math.PI / 2) {
			globe.drawLine((int) ((radius - 10) * Math.sin(i)), (int) ((radius - 10) * Math.cos(i)),
					(int) ((radius - 30) * Math.sin(i)), (int) ((radius - 30) * Math.cos(i)));
		}

		for (double i = 0; i < 2 * Math.PI; i = i + Math.PI / 6) {
			globe.drawLine((int) ((radius - 20) * Math.sin(i)), (int) ((radius - 20) * Math.cos(i)),
					(int) ((radius - 30) * Math.sin(i)), (int) ((radius - 30) * Math.cos(i)));
		}
		
		// globe.setColor(Color.pink);
		for (double i = 16 * Math.PI / 18; i < 20 * Math.PI / 18; i = i + Math.PI / 18) {
			globe.drawLine((int) ((radius - 20) * Math.sin(i)), (int) ((radius - 20) * Math.cos(i)),
					(int) ((radius - 30) * Math.sin(i)), (int) ((radius - 30) * Math.cos(i)));
		}

		// globe.setColor(Color.white);
		globe.drawLine((int) ((radius - 15) * Math.sin(3 * Math.PI / 4)),
				(int) ((radius - 15) * Math.cos(3 * Math.PI / 4)), (int) ((radius - 30) * Math.sin(3 * Math.PI / 4)),
				(int) ((radius - 30) * Math.cos(3 * Math.PI / 4)));

		globe.drawLine((int) ((radius - 15) * Math.sin(5 * Math.PI / 4)),
				(int) ((radius - 15) * Math.cos(5 * Math.PI / 4)), (int) ((radius - 30) * Math.sin(5 * Math.PI / 4)),
				(int) ((radius - 30) * Math.cos(5 * Math.PI / 4)));
		
		// globe.setColor(Color.orange);
		globe.drawLine(0, -radius + 30, 0, -radius + 10); // upright (top)
	}

	public void draw270Pointer(Graphics2D globe) {
		globe.setColor(Color.green);

		if (p270B) {
			globe.drawLine(0, 0, (int) (-radius * Math.cos(heading)),
					(int) (-radius * Math.sin(heading) * Math.sin(theta)));
			globe.setColor(Color.black);
		}
	}

	public void drawData(Graphics2D globe) {
		globe.setColor(Color.black);

		// Date resultDate = new Date(System.currentTimeMillis());
		// globe.drawString(sdf.format(resultDate), 10, 20);

		globe.drawString("Roll  " + df1.format(Math.toDegrees(phi)), 10, 60);
		globe.drawString("Pitch " + df1.format(Math.toDegrees(theta)), 10, 80);
		globe.drawString("Heading " + df1.format(Math.toDegrees(heading)), 10, 100);
		globe.drawString("deltaT " + deltaT,  10,  120);
		globe.drawString("lCount " + loopCount, 5, 140);
		globe.drawLine(20, 160, 20, 160 + loopCount/100);
		globe.drawLine(20, 160, 25, 160);
		globe.drawString("    0", 30, 160);
		globe.drawLine(20, 160 + 50, 25, 160 + 50);
		globe.drawLine(20, 160 + 100, 25, 160 + 100);
		globe.drawString("10000", 30, 260);
		globe.drawLine(20, 160 + 150, 25, 160 + 150); 
		globe.drawLine(20, 160 + 200, 25, 160 + 200);
		globe.drawString("20000", 30, 360);
		
	}

	/* Draw lines of longitude */
	public void drawMeridians(Graphics2D globe) {

		globe.setColor(Color.darkGray);

		/* 'i' is the angle (in degrees) of the meridian to draw. */
		for (double i = 0; i < 2 * Math.PI; i = i + Math.PI / 6) {

			// Width of the meridian ellipse, bi
			int bi = (int) (radius * Math.cos(heading - (Math.PI / 2 + i)) * Math.cos(theta));

			double ar = fA2(heading - i);
			globe.rotate(ar);

			/*
			 * If the dot product is positive, the meridian to draw is within 90
			 * degrees of the heading. We don't want meridians on the opposite
			 * side to the globe drawn.
			 */
			if (Math.sin(heading) * Math.sin(i) + Math.cos(heading) * Math.cos(i) > 0.0) {
				globe.drawArc(-radius, bi, 2 * radius, 2 * -bi, 0, 180);
				globe.drawArc(-radius, -bi, 2 * radius, 2 * bi, 180, 180);
			}
			globe.rotate(-ar);
		}
	}

	/*
	 * Takes difference between heading and i, returns an angle. Used in
	 * drawMeridians to calculate angle major axis of meridian ellipse makes
	 * with the x axis.
	 */
	public double fA2(double h) {

		return Math.PI / 2 + Math.atan(Math.sin(theta) * Math.sin(h) / Math.cos(h));

	}

	/* Draw the meridian numbers on the meridians */
	public void drawNumbers(Graphics2D globe) {
		globe.setColor(Color.black);
		for (double i = 0; i < 2 * Math.PI; i = i + Math.PI / 6) {

			double meridian = 3 * Math.PI / 2 - i;
			if (meridian < 0) {
				meridian = Math.PI * 2 + meridian;
			}
			/* dot product to only draw headings on the near side of the globe */
			if (Math.sin(heading) * Math.sin(meridian) + Math.cos(heading) * Math.cos(meridian) > 0.5) {
				globe.drawString(df1.format(Math.toDegrees(meridian)), (int) (-radius * Math.cos(heading + i)) + 3,
						(int) (-radius * Math.sin(heading + i) * Math.sin(theta)));
			}
		}
	}

	public void drawPoles(Graphics2D globe) {
		// North pole
		if (theta >= 0) {
			northY = -(int) (radius * Math.cos(theta));
			globe.setColor(Color.red);
			globe.fillOval(-2, +northY, 4, 4);
		}
		// South pole
		if (theta < 0) {
			globe.setColor(Color.blue);
			southY = (int) (radius * Math.cos(theta));
			globe.fillOval(-2, -2 + southY, 4, 4);
		}
	}

	/* Draw lines of latitude */
	public void drawParallels(Graphics2D globe) {
		int delta;

		// latitude 0
		int n0y = (int) (radius * Math.sin(0));
		n0y = -2 - (int) (n0y * Math.cos(theta));
		globe.fillOval(-2, n0y, 4, 4);
		int r0 = (int) (radius * Math.cos(0));
		int r0y = (int) (r0 * Math.sin(theta));

		skyArc = new Arc2D.Double(-r0, n0y - r0y, 2 * r0, 2 * r0y, 0, -180, 1);
		sky = new Area(skyArc);
		globe.setColor(Color.lightGray);
		globe.fill(sky);
		skyTop = new Arc2D.Double(-radius, -radius, 2 * radius, 2 * radius, 0, 180, 1);
		sky = new Area(skyTop);
		globe.fill(sky);
		if (theta < 0) {
			groundArc = new Arc2D.Double(-r0, n0y + r0y + 2, 2 * r0, -2 * r0y, 0, 180, 1);
			sky = new Area(groundArc);
			globe.setColor(Color.gray);
			globe.fill(sky);
		}

		globe.setColor(Color.black);
		if (theta > 0) {
			delta = 0;
			globe.drawArc(-r0, n0y - r0y, 2 * r0, 2 * r0y, 0, -180);
		} else {
			globe.drawArc(-r0, n0y + r0y + 2, 2 * r0, -2 * r0y, 0, 180);
		}

		if (parallelB) {
			// Northern parallels
			globe.setColor(Color.blue);
			for (int i = 10; i < 90; i = i + 10) {
				int nny = (int) (radius * Math.sin(Math.toRadians(i)));
				nny = -(int) (nny * Math.cos(theta));

				int rn = (int) (radius * Math.cos(Math.toRadians(i)));
				int rny = (int) (rn * Math.sin(theta));
				
				// Math
				delta = 90 - (int) Math.toDegrees(Math.acos(Math.sin(Math.toRadians(i)) * Math.sin(theta)
						/ (Math.cos(Math.toRadians(i)) * Math.cos(theta))));
				
				if (theta >= 0) {
					globe.drawArc(-rn, nny - rny, 2 * rn, 2 * rny, delta, -180 - 2 * delta);
				} else if (Math.toDegrees(theta) > (-90 + i)) {

					globe.drawArc(-rn, nny + rny, 2 * rn, -2 * rny, -delta, 180 + 2 * delta);
				}
			}

			// Southern parallels
			globe.setColor(Color.black);
			for (int i = -10; i > -90; i = i - 10) {
				int n_ly = (int) (radius * Math.sin(Math.toRadians(i)));
				n_ly = -(int) (n_ly * Math.cos(theta));

				int r_l = (int) (radius * Math.cos(Math.toRadians(i)));
				int r_ly = (int) (r_l * Math.sin(theta));
				
				// Math
				delta = 90 - (int) Math.toDegrees((Math.acos(Math.sin(Math.toRadians(i)) * Math.sin(theta)
						/ (Math.cos(Math.toRadians(i)) * Math.cos(theta)))));
				
				if (theta >= 0 && Math.toDegrees(theta) < (90 + i)) {
					globe.drawArc(-r_l, n_ly - r_ly, 2 * r_l, 2 * r_ly, delta, -180 - 2 * delta);
				} else if (theta < (90 - i)) {
					globe.drawArc(-r_l, n_ly + r_ly, 2 * r_l, -2 * r_ly, -delta, 180 + 2 * delta);
				}
			}
		}
	}

	/* Draw the aircraft attitude indicator pipper */
	public void drawAircraft(Graphics2D ball) {
		ball.setColor(Color.magenta);
		ball.drawLine(-16, 0, 16, 0); // aircraft wings
		ball.drawLine(0, 0, 0, -10); // aircraft tail
		ball.setColor(Color.black);
		ball.fillOval(-2, -2, 4, 4); // aircraft nose
	}

	public void drawButtons(Graphics2D globe) {
		globe.fillRect(instrumentX, instrumentY, buttX, buttY);
		globe.setColor(Color.cyan);
		globe.drawString("instrument", instrumentX + 2, instrumentY + 15);
		if (instrumentB) {
			globe.drawRect(instrumentX + 1, instrumentY + 1, buttX - 3, buttY - 3);
		}

		globe.setColor(Color.black);
		globe.fillRect(meridianX, meridianY, buttX, buttY);
		globe.setColor(Color.cyan);
		globe.drawString("meridians", meridianX + 5, meridianY + 15);
		if (meridianB) {
			globe.drawRect(meridianX + 1, meridianY + 1, buttX - 3, buttY - 3);
		}

		globe.setColor(Color.black);
		globe.fillRect(p270X, p270Y, buttX, buttY);
		globe.setColor(Color.cyan);
		globe.drawString("p270", p270X + 19, p270Y + 15);
		if (p270B) {
			globe.drawRect(p270X + 1, p270Y + 1, buttX - 3, buttY - 3);
		}

		globe.setColor(Color.black);
		globe.fillRect(parallelX, parallelY, buttX, buttY);
		globe.setColor(Color.cyan);
		globe.drawString("parallels", parallelX + 6, parallelY + 15);
		if (parallelB) {
			globe.drawRect(parallelX + 1, parallelY + 1, buttX - 3, buttY - 3);
		}

		globe.setColor(Color.black);
		globe.fillRect(numberX, numberY, buttX, buttY);
		globe.setColor(Color.cyan);
		globe.drawString("numbers", numberX + 6, numberY + 15);
		if (numberB) {
			globe.drawRect(numberX + 1, numberY + 1, buttX - 3, buttY - 3);
		}
	}

	public void DrawBackbufferToScreen() {
		bufferStrategy.show();
		Toolkit.getDefaultToolkit().sync();
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		int bx = e.getX();
		int by = e.getY();

		if (bx >= instrumentX && bx <= instrumentX + buttX && by >= instrumentY && by <= instrumentY + buttY) {
			instrumentB ^= true;
		}

		if (bx >= meridianX && bx <= meridianX + buttX && by >= meridianY && by <= meridianY + buttY) {
			meridianB ^= true;
		}

		if (bx >= parallelX && bx <= parallelX + buttX && by >= parallelY && by <= parallelY + buttY) {
			parallelB ^= true;
		}

		if (bx >= p270X && bx <= p270X + buttX && by >= p270Y && by <= p270Y + buttY) {
			p270B ^= true;
		}

		if (bx >= numberX && bx <= numberX + buttX && by >= numberY && by <= numberY + buttY) {
			numberB ^= true;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (!keysDown.contains(e.getKeyCode()))
			keysDown.add(new Integer(e.getKeyCode()));
		// System.out.println(e.getKeyCode());
		adjust();
	}

	public void adjust() {

		int pitchUp = 98; // 2
		int pitchDown = 104; // 8
		int rollLeft = 100; // 4
		int rollRight = 102; // 6
		int yawLeft = 97; // 1
		int yawRight = 99; // 3

		int yawRatePlus = 105; // 9
		int yawRateSubtract = 103; // 7
		
		int zeroRates = 101; // 5
		
		double rate = 0.0001;

		if (keysDown.contains(rollLeft)) {
			ux = a11;
			uy = a21;
			uz = a31; // defines the unit vector for roll
			rotate(Math.PI / 180);
		}
		if (keysDown.contains(rollRight)) {
			ux = a11;
			uy = a21;
			uz = a31; // defines the unit vector for roll
			rotate(-Math.PI / 180);
		}

		if (keysDown.contains(pitchUp)) {
			ux = a12;
			uy = a22;
			uz = a32; // set unit vector to select a pitch rotation
			rotate(-Math.PI / 180);
		}
		if (keysDown.contains(pitchDown)) {
			ux = a12;
			uy = a22;
			uz = a32; // set unit vector to select a pitch rotation
			rotate(Math.PI / 180);
		}

		if (keysDown.contains(yawLeft)) {
			ux = a13;
			uy = a23;
			uz = a33; // defines the unit vector for yaw
			rotate(-Math.PI / 180);
		}
		if (keysDown.contains(yawRight)) {
			ux = a13;
			uy = a23;
			uz = a33; // defines the unit vector for yaw
			rotate(Math.PI / 180);
		}
		if (keysDown.contains(new Integer(KeyEvent.VK_UP))) {
			dPitch += rate; // half a degree
		}
		if (keysDown.contains(new Integer(KeyEvent.VK_DOWN))) {
			dPitch -= rate; // half a degree
		}
		if (keysDown.contains(new Integer(KeyEvent.VK_LEFT))) {
			dRoll += rate; // half a degree
		}
		if (keysDown.contains(new Integer(KeyEvent.VK_RIGHT))) {
			dRoll -= rate; // half a degree
		}
		if (keysDown.contains(yawRatePlus)) {
			dYaw += rate;
		}
		if (keysDown.contains(yawRateSubtract)) {
			dYaw -= rate;
		}
		if (keysDown.contains(zeroRates)){
			dPitch = 0.0;
			dRoll = 0.0;
			dYaw = 0.0;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keysDown.remove(new Integer(e.getKeyCode()));
	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}

	// set unit vector to select a pitch rotation
	public void pitch() {
		ux = a12;
		uy = a22;
		uz = a32; 
		rotate(dPitch);
	}

	// defines the unit vector for roll
	public void roll() {
		ux = a11;
		uy = a21;
		uz = a31; 
		rotate(dRoll);
	}

	// defines the unit vector for yaw
	public void yaw() {
		ux = a13;
		uy = a23;
		uz = a33; 
		rotate(dYaw);
	}

	
	/* R matrix is the rotation matrix. ux, uy, uz are the unit vectors. 
	 * omega is the angular velocity */
	public void rotate(double omega) {
		// Define R, the rotation matrix
		r11 = Math.cos(omega) + ((ux * ux) * (1.0 - Math.cos(omega)));
		r12 = ((ux * uy) * (1.0 - Math.cos(omega)) - (uz * Math.sin(omega)));
		r13 = ((ux * uz) * (1.0 - Math.cos(omega)) + (uy * Math.sin(omega)));

		r21 = ((ux * uy) * (1.0 - Math.cos(omega)) + (uz * Math.sin(omega)));
		r22 = Math.cos(omega) + ((uy * uy) * (1.0 - Math.cos(omega)));
		r23 = ((uy * uz) * (1.0 - Math.cos(omega)) - (ux * Math.sin(omega)));

		r31 = ((ux * uz) * (1.0 - Math.cos(omega)) - (uy * Math.sin(omega)));
		r32 = ((uz * uy) * (1.0 - Math.cos(omega)) + (ux * Math.sin(omega)));
		r33 = Math.cos(omega) + ((uz * uz) * (1.0 - Math.cos(omega)));

		// Perform the matrix multiplication: RA = B
		b11 = (r11 * a11) + (r12 * a21) + (r13 * a31);
		b12 = (r11 * a12) + (r12 * a22) + (r13 * a32);
		b13 = (r11 * a13) + (r12 * a23) + (r13 * a33);
		b21 = (r21 * a11) + (r22 * a21) + (r23 * a31);
		b22 = (r21 * a12) + (r22 * a22) + (r23 * a32);
		b23 = (r21 * a13) + (r22 * a23) + (r23 * a33);
		b31 = (r31 * a11) + (r32 * a21) + (r33 * a31);
		b32 = (r31 * a12) + (r32 * a22) + (r33 * a32);
		b33 = (r31 * a13) + (r32 * a23) + (r33 * a33);

		// copy the temporary matrix elements into A
		a11 = b11;
		a12 = b12;
		a13 = b13;
		a21 = b21;
		a22 = b22;
		a23 = b23;
		a31 = b31;
		a32 = b32;
		a33 = b33;
	}

}