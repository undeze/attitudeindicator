/** 
 * Attitude Indicator
 * Graphics2D
 * Chris Underwood 2018
 * New Zealand
 * undeze@gmail.com
 * 
 * **/

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;


public class Start extends JFrame{
	
	public Start(){
		super();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension instrumentsSize = new Dimension(600, 440);
        this.setSize(instrumentsSize);
	}
	
	public static void main(String[] args) {
		Start instruments = new Start();
        Container contentPane = instruments.getContentPane();
        contentPane.setLayout(new GridLayout(1,1));
        AttitudeIndicator AI = new AttitudeIndicator(instruments.getSize());
        contentPane.add(AI);
        instruments.setVisible(true);
	}
}
